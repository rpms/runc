%global with_debug 1
%global with_bundled 1
%global with_check 0

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%if 0%{?rhel} > 7 && ! 0%{?fedora}
%define gobuild(o:) \
go build -buildmode pie -compiler gc -tags="rpm_crashtraceback no_openssl ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -compressdwarf=false -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags'" -a -v -x %{?**};
%endif # distro

%global provider github
%global provider_tld com
%global project opencontainers
%global repo runc
# https://github.com/opencontainers/runc
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path %{provider_prefix}
%global git0 https://github.com/opencontainers/runc
%global commit0 2abd837c8c25b0102ac4ce14f17bc0bc7ddffba7
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name: %{repo}
Version: 1.0.0
Release: 56.rc5.dev.git%{shortcommit0}%{?dist}
Summary: CLI for running Open Containers
ExcludeArch: %{ix86}
License: ASL 2.0
URL: http//%{provider_prefix}
Source0: %{git0}/archive/%{commit0}/%{repo}-%{shortcommit0}.tar.gz
Source1: 99-containers.conf
Patch0: change-default-root.patch
Patch1: 0001-Revert-Apply-cgroups-earlier.patch
Patch2: 1807.patch
Patch3: 0001-nsenter-clone-proc-self-exe-to-avoid-exposing-host-b-runc.patch
Patch4: pivot-root.patch
Requires: criu
Requires(pre): container-selinux >= 2:2.2-2

# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang} >= 1.6.2
BuildRequires: git
BuildRequires: go-md2man
BuildRequires: libseccomp-devel

%description
The runc command can be used to start containers which are packaged
in accordance with the Open Container Initiative's specifications,
and to manage containers running under runc.

%prep
%autosetup -Sgit -n %{repo}-%{commit0}
sed -i '/\#\!\/bin\/bash/d' contrib/completions/bash/%{name}

%build
mkdir -p GOPATH
pushd GOPATH
    mkdir -p src/%{provider}.%{provider_tld}/%{project}
    ln -s $(dirs +1 -l) src/%{import_path}
popd

pushd GOPATH/src/%{import_path}
export GOPATH=%{gopath}:$(pwd)/GOPATH
export BUILDTAGS="selinux seccomp"
%gobuild -o %{name} %{import_path} 

pushd man
./md2man-all.sh
popd

%install
install -d -p %{buildroot}%{_bindir}
install -p -m 755 %{name} %{buildroot}%{_bindir}

# install man pages
install -d -p %{buildroot}%{_mandir}/man8
install -p -m 644 man/man8/* %{buildroot}%{_mandir}/man8
# install bash completion
install -d -p %{buildroot}%{_datadir}/bash-completion/completions
install -p -m 0644 contrib/completions/bash/%{name} %{buildroot}%{_datadir}/bash-completion/completions

%check

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc MAINTAINERS_GUIDE.md PRINCIPLES.md README.md CONTRIBUTING.md
%{_bindir}/%{name}
%{_mandir}/man8/%{name}*
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Thu Nov 28 2019 Jindrich Novy <jnovy@redhat.com> - 1.0.0-56.rc5.dev.git2abd837
- rebuild because of CVE-2019-9512 and CVE-2019-9514
- Resolves: #1766328, #1766300

* Tue Feb 12 2019 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-55.rc5.dev.git2abd837
- Resolves: #1665770 - rootfs: umount all procfs and sysfs with --no-pivot
- Resolves: CVE-2019-5736

* Tue Dec 18 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-54.rc5.dev.git2abd837
- re-enable debuginfo

* Mon Dec 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-53.rc5.dev.git2abd837
- go toolset not in scl anymore

* Wed Sep 26 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-52.rc5.dev.git2abd837
- rebase

* Fri Aug 31 2018 Dan Walsh <dwalsh@redhat.name> - 2:1.0.0-51.dev.gitfdd8055
- Fix handling of tmpcopyup

* Fri Aug 24 2018 Lokesh Mandvekar <lsm5@redhat.com> - 2:1.0.0-49.rc5.dev.gitb4e2ecb
- %%gobuild uses no_openssl
- remove unused devel and unit-test subpackages

* Tue Aug 07 2018 Lokesh Mandvekar <lsm5@redhat.com> - 2:1.0.0-48.rc5.dev.gitad0f525
- build with %%gobuild
- exlude i686 temporarily because of go-toolset issues

* Mon Jul 30 2018 Florian Weimer <fweimer@redhat.com> - 1.0.0-47.dev.gitb4e2ecb
- Rebuild with fixed binutils

* Fri Jul 27 2018 Dan Walsh <dwalsh@redhat.name> - 2:1.0.0-46.dev.gitb4e2ecb
- Add patch https://github.com/opencontainers/runc/pull/1807 to allow
- runc and podman to work with sd_notify

* Wed Jul 18 2018 Dan Walsh <dwalsh@redhat.com> - 2:1.0.0-40.rc5.dev.gitad0f525
- Remove sysclt handling, not needed in RHEL8
- Make sure package built with seccomp flags
- Remove rectty
- Add completions

* Fri Jun 15 2018 Dan Walsh <dwalsh@redhat.com> - 2:1.0.0-36.rc5.dev.gitad0f525
- Better handling of user namespace

* Tue May 1 2018 Dan Walsh <dwalsh@redhat.name> - 2:1.0.0-31.rc5.git0cbfd83
- Fix issues between SELinux and UserNamespace

* Tue Apr 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-27.rc5.dev.git4bb1fe4
- rebuilt, placed missing changelog entry back

* Tue Feb 27 2018 Dan Walsh <dwalsh@redhat.name> - 2:1.0.0-26.rc5.git4bb1fe4
- release v1.0.0~rc5

* Wed Jan 24 2018 Dan Walsh <dwalsh@redhat.name> - 1.0.0-26.rc4.git9f9c962
- Bump to the latest from upstream

* Mon Dec 18 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-25.rc4.gite6516b3
- built commit e6516b3

* Fri Dec 15 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-24.rc4.dev.gitc6e4a1e.1
- rebase to c6e4a1ebeb1a72b529c6f1b6ee2b1ae5b868b14f
- https://github.com/opencontainers/runc/pull/1651

* Tue Dec 12 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-23.rc4.git1d3ab6d
- Resolves: #1524654

* Sun Dec 10 2017 Dan Walsh <dwalsh@redhat.name> - 1.0.0-22.rc4.git1d3ab6d
- Many Stability fixes
- Many fixes for rootless containers
- Many fixes for static builds

* Thu Nov 09 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-21.rc4.dev.gitaea4f21
- enable debuginfo and include -buildmode=pie for go build

* Tue Nov 07 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-20.rc4.dev.gitaea4f21
- use Makefile

* Tue Nov 07 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-19.rc4.dev.gitaea4f21
- disable debuginfo temporarily

* Fri Nov 03 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-18.rc4.dev.gitaea4f21
- enable debuginfo

* Wed Oct 25 2017 Dan Walsh <dwalsh@redhat.name> - 1.0.0-17.rc4.gitaea4f21
- Add container-selinux prerequires to make sure runc is labeled correctly

* Thu Oct 19 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-16.rc4.dev.gitaea4f21
- correct the release tag "rc4dev" -> "rc4.dev" cause I'm OCD

* Mon Oct 16 2017 Dan Walsh <dwalsh@redhat.com> - 1.0.0-15.rc4dev.gitaea4f21
- Use the same checkout as Fedora for lates CRI-O

* Fri Sep 22 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-14.rc4dev.git84a082b
- rebase to 84a082bfef6f932de921437815355186db37aeb1

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-13.rc3.gitd40db12
- Resolves: #1479489
- built commit d40db12

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-12.1.gitf8ce01d
- disable s390x temporarily because of indefinite wait times on brew

* Tue Jun 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-11.1.gitf8ce01d
- correct previous bogus date :\

* Mon Jun 12 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-10.1.gitf8ce01d
- Resolves: #1441737 - run sysctl_apply for sysctl knob

* Tue May 09 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-9.1.gitf8ce01d
- Resolves: #1447078 - change default root path
- add commit e800860 from runc @projectatomic/change-root-path

* Fri May 05 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-8.1.gitf8ce01d
- Resolves: #1441737 - enable kernel sysctl knob /proc/sys/fs/may_detach_mounts

* Thu Apr 13 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-7.1.gitf8ce01d
- Resolves: #1429675
- built @opencontainers/master commit f8ce01d

* Thu Mar 16 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-4.1.gitee992e5
- built @projectatomic/master commit ee992e5

* Fri Feb 24 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-3.rc2
- Resolves: #1426674
- built projectatomic/runc_rhel_7 commit 5d93f81

* Mon Feb 06 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-2.rc2
- Resolves: #1419702 - rebase to latest upstream master
- built commit b263a43

* Wed Jan 11 2017 Lokesh Mandvekar <lsm5@redhat.com> - 1.0.0-1.rc2
- Resolves: #1412239 - *CVE-2016-9962* - set init processes as non-dumpable,
runc patch from Michael Crosby <crosbymichael@gmail.com>

* Wed Sep 07 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-6
- Resolves: #1373980 - rebuild for 7.3.0

* Sat Jun 25 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-5
- build with golang >= 1.6.2

* Tue May 31 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-4
- release tags were inconsistent in the previous build

* Tue May 31 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.1-1
- Resolves: #1341267 - rebase runc to v0.1.1

* Tue May 03 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-3
- add selinux build tag
- add BR: libseccomp-devel

* Tue May 03 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-2
- Resolves: #1328970 - add seccomp buildtag

* Tue Apr 19 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.1.0-1
- Resolves: rhbz#1328616 - rebase to v0.1.0

* Tue Mar 08 2016 Lokesh Mandvekar <lsm5@redhat.com> - 0.0.8-1.git4155b68
- Resolves: rhbz#1277245 - bump to 0.0.8
- Resolves: rhbz#1302363 - criu is a runtime dep
- Resolves: rhbz#1302348 - libseccomp-golang is bundled in Godeps
- manpages included

* Wed Nov 25 2015 jchaloup <jchaloup@redhat.com> - 1:0.0.5-0.1.git97bc9a7
- Update to 0.0.5, introduce Epoch for Fedora due to 0.2 version instead of 0.0.2

* Fri Aug 21 2015 Jan Chaloupka <jchaloup@redhat.com> - 0.2-0.2.git90e6d37
- First package for Fedora
  resolves: #1255179
